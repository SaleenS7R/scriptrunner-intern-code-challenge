package com.adaptavist

import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import groovy.json.JsonBuilder
import groovy.transform.BaseScript
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response
import com.atlassian.jira.component.ComponentAccessor
import org.ofbiz.core.entity.ConnectionFactory
import org.ofbiz.core.entity.DelegatorInterface
import java.sql.Connection
import java.sql.ResultSet
import java.sql.Statement

@BaseScript CustomEndpointDelegate delegate

getDomains(httpMethod: "GET", groups: ["jira-administrators"]) {
    MultivaluedMap queryParams, String body ->
        def delegator = (DelegatorInterface) ComponentAccessor.getComponent(DelegatorInterface);
        String helperName = delegator.getGroupHelperName("default");
        //Getting DB connector
        Connection conn = ConnectionFactory.getConnection(helperName);
        ArrayList<String> domains = new ArrayList<>();
        Statement stmt = null;
        
        //Creating query and executing query
        String query = "SELECT EMAIL_ADDRESS FROM CWD_USER";
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                String st = rs.getString("EMAIL_ADDRESS");
                String domain = st.substring(st.indexOf('@') + 1);
                //if this domain was not previously stored in domains then it's stored
                if (!domains.contains(domain)) {
                    domains.add(domain);
                }
            }
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        String result = "";
        //Building response: response is a plain String, could be JSON, XML or other format if desired
        for (String domain : domains) {
            if (result.length() != 0){
                result += ", ";
            }
            result += domain;
        }
        return Response.ok(new JsonBuilder(result).toString()).build();
}